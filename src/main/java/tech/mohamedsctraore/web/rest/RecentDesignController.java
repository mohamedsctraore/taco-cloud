package tech.mohamedsctraore.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import tech.mohamedsctraore.domain.Order;
import tech.mohamedsctraore.domain.Taco;
import tech.mohamedsctraore.repository.OrderRepository;
import tech.mohamedsctraore.repository.TacoRepository;

import java.util.List;

/**
 * Created by mohamed on 7/1/19.
 */
@RestController
@RequestMapping(path = "/design", produces = "application/json")
@CrossOrigin("*")
public class RecentDesignController {

    private final TacoRepository tacoRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public RecentDesignController(TacoRepository tacoRepository, OrderRepository orderRepository) {
        this.tacoRepository = tacoRepository;
        this.orderRepository = orderRepository;
    }

    @GetMapping("/recent")
    public Iterable<Taco> recentTacos() {
        PageRequest pageRequest = PageRequest.of(0, 12, Sort.by("createdAt").descending());
        return tacoRepository.findAll(pageRequest).getContent();
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Taco postTaco(@RequestBody Taco taco) {
        return tacoRepository.save(taco);
    }

    @PutMapping("/{orderId}")
    public Order putOrder(@RequestBody Order order) {
        return orderRepository.save(order);
    }

    @DeleteMapping("/{orderId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteOrder(@PathVariable("orderId") Long orderId) {
        orderRepository.deleteById(orderId);
    }

    @GetMapping("/recents")
    public Resources<Resource<Taco>> recentsTacos() {
        PageRequest page = PageRequest.of(0, 12, Sort.by("createdAt").descending());
        List<Taco> tacoList = tacoRepository.findAll(page).getContent();

        Resources<Resource<Taco>> wrap = Resources.wrap(tacoList);
        wrap.add(new Link("http://localhost:8080/design/recent", "recents"));

        return wrap;
    }
}
