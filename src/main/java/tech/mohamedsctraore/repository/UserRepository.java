package tech.mohamedsctraore.repository;

import org.springframework.data.repository.CrudRepository;
import tech.mohamedsctraore.domain.User;

/**
 * Created by mohamed on 6/30/19.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
