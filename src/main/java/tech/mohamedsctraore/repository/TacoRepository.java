package tech.mohamedsctraore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import tech.mohamedsctraore.domain.Taco;

/**
 * Created by mohamed on 6/30/19.
 */
public interface TacoRepository extends PagingAndSortingRepository<Taco, Long> {
}
