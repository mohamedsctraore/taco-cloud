package tech.mohamedsctraore.repository;

import org.springframework.data.repository.CrudRepository;
import tech.mohamedsctraore.domain.Ingredient;

/**
 * Created by mohamed on 6/30/19.
 */
public interface IngredientRepository extends CrudRepository<Ingredient, String >{
}
